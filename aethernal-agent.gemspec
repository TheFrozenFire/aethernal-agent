
lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "aethernal_agent/version"

Gem::Specification.new do |spec|
  spec.name          = "aethernal-agent"
  spec.version       = AethernalAgent::VERSION
  spec.authors       = ["Maran"]
  spec.email         = ["maran@bytesized-hosting.com"]

  spec.summary       = %q{Aethernal Agent for Aethernal.host.}
  spec.description   = %q{Install and configure apps on the go.}
  spec.homepage      = "https://aethernal.host"

  # Prevent pushing this gem to RubyGems.org. To allow pushes either set the 'allowed_push_host'
  # to allow pushing to a single host or delete this section to allow pushing to any host.
  if spec.respond_to?(:metadata)
    spec.metadata["homepage_uri"] = spec.homepage
    spec.metadata["source_code_uri"] = "http://gitlab.com/olaris/aethernal_agent"
    spec.metadata["changelog_uri"] = "http://gitlab.com/olaris/aethernal_agent"
  else
    raise "RubyGems 2.0 or newer is required to protect against " \
      "public gem pushes."
  end

  # Specify which files should be added to the gem when it is released.
  # The `git ls-files -z` loads the files in the RubyGem that have been added into git.
  spec.files         = Dir.chdir(File.expand_path('..', __FILE__)) do
    `git ls-files -z`.split("\x0").reject { |f| f.match(%r{^(test|spec|features)/}) }
  end
  spec.bindir        = "exe"
  spec.executables   = spec.files.grep(%r{^exe/}) { |f| File.basename(f) }
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", ">= 2.0"
  spec.add_development_dependency "rake", "~> 10.5"
  spec.add_development_dependency "serverspec"
  spec.add_development_dependency "pry"
  spec.add_dependency "activesupport" # We leave this loose to not interfere with Rails projects
  spec.add_dependency "ruby2_keywords", "0.0.2" # We leave this loose to not interfere with Rails projects
  spec.add_dependency "sinatra", "~> 2.0.7"
  spec.add_dependency "sinatra-contrib"
  spec.add_dependency "sqlite3"
  spec.add_dependency "bcrypt"
  spec.add_dependency "docker-api", "~> 1.34.2"
  spec.add_dependency "thor"
  spec.add_dependency "terminal-table"
end
