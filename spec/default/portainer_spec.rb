require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Portainer installation" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test portainer remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test portainer configure_app_user testuser \'{"port":58917, "docker_socket": "/var/run/docker.sock","data_volume": "/tmp/test"}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe docker_image('portainerci/portainer:develop') do
    it { should exist }
  end

  describe docker_container('testuser.portainer') do
    it { should exist }
    it { should have_volume('/data', '/tmp/test') }
    it { should be_running }
  end

  describe file("/home/testuser/.config/systemd/user/portainer.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    its(:content) { should match /testuser.portainer/ }
  end

end
