require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Jackett user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test jackett remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test jackett configure_app_user testuser \'{"port": 2865}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe file("/home/testuser/.config/systemd/user/jackett.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/jackett") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  # describe file("/home/testuser/apps/jackett/SickBeard.py") do
  #   it { should be_file }
  #   it { should be_owned_by "testuser" }
  # end

  describe file("/etc/apache2/users/testuser/jackett.conf") do
    it { should be_file }
  end
end

