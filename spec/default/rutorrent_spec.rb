require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a ruTorrent Docker installation" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test rutorrent remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test rutorrent configure_app_user testuser \'{"connection_port": 39494,"config_mount": "/tmp/config", "download_mount": "/tmp/media", "password_mount":"/mnt/pass"}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe docker_image('crazymax/rtorrent-rutorrent') do
    it { should exist }
  end

  describe docker_container('testuser.rutorrent') do
    it { should exist }
    it { should have_volume('/data', '/tmp/config') }
    it { should have_volume('/downloads', '/tmp/media') }
    it { should have_volume('/passwd', '/mnt/pass') }
    it { should be_running }
  end

  describe file("/home/testuser/.config/systemd/user/rutorrent.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    its(:content) { should match /testuser.rutorrent/ }
  end

  describe file("/mnt/pass/rpc.htpasswd") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    its(:content) { should match /testuser/ }
  end

  describe file("/tmp/config/rtorrent/.rtorrent.rc") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    its(:content) { should match /39494-39494/ }
  end

end
