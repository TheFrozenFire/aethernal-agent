require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a filebrowser user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test filebrowser remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test filebrowser configure_app_user testuser \'{"port": 5959}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe process("filebrowser") do
    its(:user) { should eq "testuser" }
  end

  describe port(5959) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/filebrowser") do
    it { should be_directory }
  end

  describe file("/home/testuser/.config/systemd/user/filebrowser.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/filebrowser") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/filebrowser/linux-amd64-filebrowser.tar.gz") do
    it { should_not exist }
  end

  describe file("/home/testuser/apps/filebrowser/filebrowser") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    it { should be_executable }
  end

  describe file("/etc/apache2/users/testuser/filebrowser.conf") do
    it { should be_file }
  end
end
