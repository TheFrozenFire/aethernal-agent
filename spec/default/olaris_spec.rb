require 'spec_helper'
require 'aethernal_agent'

describe "Configuring an Olaris user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test olaris remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test olaris configure_app_user testuser \'{"port": 5555}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe process("olaris") do
    its(:user) { should eq "testuser" }
  end

  describe port(5555) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/olaris") do
    it { should be_directory }
  end

  describe file("/home/testuser/.config/systemd/user/olaris.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/olaris") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/olaris/olaris.zip") do
    it { should_not exist }
  end

  describe file("/home/testuser/apps/olaris/bin/olaris") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    it { should be_executable }
  end

  describe file("/etc/apache2/users/testuser/olaris.conf") do
    it { should be_file }
  end
end
