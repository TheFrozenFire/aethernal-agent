require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Medusa user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test medusa remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test medusa configure_app_user testuser \'{"port": 5145}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe port(5145) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/medusa") do
    it { should be_directory }
  end

  describe file("/home/testuser/.config/systemd/user/medusa.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/medusa") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/medusa/SickBeard.py") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/etc/apache2/users/testuser/medusa.conf") do
    it { should be_file }
  end
end
