require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Jdownloader user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test jdownloader remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test jdownloader configure_app_user testuser \'{ "password": "hallo", "email": "test@test.nl", "domain_name": "test.aethernal.host"}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe file("/home/testuser/.config/jdownloader") do
    it { should be_directory }
  end

  describe file("/home/testuser/apps/jdownloader/cfg/org.jdownloader.api.myjdownloader.MyJDownloaderSettings.json") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    it { should contain 'password' }
    it { should contain 'test-container' }
    it { should contain 'hallo' }
  end

  describe file("/home/testuser/.config/systemd/user/jdownloader.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe process("java") do
    its(:args) { should match /JDownloader/ }
  end

  describe file("/home/testuser/apps/jdownloader") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

end
