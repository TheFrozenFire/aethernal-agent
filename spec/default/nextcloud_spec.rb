require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Medusa user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test nextcloud remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test nextcloud configure_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe file("/home/testuser/www/nextcloud") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/www/nextcloud/index.php") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/etc/apache2/users/testuser/nextcloud.conf") do
    it { should be_file }
  end
end
