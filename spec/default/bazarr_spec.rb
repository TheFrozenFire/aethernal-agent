require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Bazarr user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test bazarr remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test bazarr configure_app_user testuser \'{"port": 5345, "password": "hallo"}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe port(5345) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/bazarr") do
    it { should be_directory }

  end

  describe file("/home/testuser/.config/bazarr/data/config/config.ini") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    it { should contain 'password = 598d4c200461b81522a3328565c25f7c' }
  end

  describe file("/home/testuser/.config/systemd/user/bazarr.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/bazarr") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/bazarr/bazarr.py") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/etc/apache2/users/testuser/bazarr.conf") do
    it { should be_file }
  end
end
