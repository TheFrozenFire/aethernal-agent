require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Sickchill user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test sickchill remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test sickchill configure_app_user testuser \'{"port": 4534}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe file("/home/testuser/.config/sickchill") do
    it { should be_directory }
  end

  describe file("/home/testuser/.config/systemd/user/sickchill.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/sickchill") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/sickchill/SickBeard.py") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/etc/apache2/users/testuser/sickchill.conf") do
    it { should be_file }
  end
end
