require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Plex installation" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test plex remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test plex configure_app_user testuser \'{"port": 9960, "plex_port": 9959, "claim_token": "imnotvalid"}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe port(32400) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/systemd/user/plex.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/var/lib/plexmediaserver") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/usr/lib/plexmediaserver") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/var/lib/plexmediaserver/Library/Application Support/Plex Media Server/Preferences.xml") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/.config/systemd/user/plex.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/lib/systemd/system/plexmediaserver.service") do
    it { should_not exist }
  end

  describe file("/etc/apache2/users/testuser/plex.conf") do
    it { should be_file }
    its(:content) { should match /32400/ }
  end

  describe package('plexmediaserver') do
    it { should be_installed }
  end
end

describe "Removing a Plex installation" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test plex remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe package('plexmediaserver') do
    it { should_not be_installed }
  end
end
