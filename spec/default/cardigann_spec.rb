require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Cardigann user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test cardigann remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test cardigann configure_app_user testuser \'{"port": 5355, "password": "hallo"}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe port(5355) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/cardigann") do
    it { should be_directory }
  end

  describe file("/home/testuser/.config/cardigann/config.json") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/.config/systemd/user/cardigann.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/cardigann") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/cardigann/cardigann") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/etc/apache2/users/testuser/cardigann.conf") do
    it { should be_file }
  end
end
