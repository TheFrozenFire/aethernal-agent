require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a gitea user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test gitea remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test gitea configure_app_user testuser \'{"port": 5942, "ssh_host_port": 2222}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe process("gitea") do
    its(:user) { should eq "testuser" }
  end

  # TODO: Why is this not working even though the port is open
  #  describe port(5942) do
  #    it { should be_listening }
  #  end

  describe file("/home/testuser/.config/gitea") do
    it { should be_directory }
  end

  describe file("/home/testuser/.config/gitea/app.ini") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/.config/systemd/user/gitea.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/gitea") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/gitea/gitea") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    it { should be_executable }
  end

  describe file("/etc/apache2/users/testuser/gitea.conf") do
    it { should be_file }
  end
end
