require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Plex Docker installation" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test plex_docker remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test plex_docker configure_app_user testuser \'{"port": 3360, "host_port": "3811", "plex_port": 3359, "claim_token": "imnotvalid", "config_mount": "/tmp/config", "media_mount": "/tmp/media", "transcode_mount":"/mnt/transcode"}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe docker_image('plexinc/pms-docker') do
    it { should exist }
  end

  describe docker_container('testuser.plex_docker') do
    it { should exist }
    it { should have_volume('/config', '/tmp/config') }
    it { should have_volume('/data', '/tmp/media') }
    it { should have_volume('/transcode', '/mnt/transcode') }
    it { should be_running }
  end

  describe file("/home/testuser/.config/systemd/user/plex_docker.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    its(:content) { should match /testuser.plex_docker/ }
  end

  describe file("/etc/apache2/users/testuser/plex_docker.conf") do
    it { should be_file }
    its(:content) { should match /3360/ }
  end

  describe file("/tmp/config/Library/Application Support/Plex Media Server/Preferences.xml") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    its(:content) { should match /3811/ }
  end

end
