require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Sonarr user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test sonarr remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test sonarr configure_app_user testuser \'{"port": 5588}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe process("mono") do
    its(:user) { should eq "testuser" }
  end

  describe port(5588) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/Sonarr") do
    it { should be_directory }
  end

  describe file("/home/testuser/.config/systemd/user/sonarr.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/sonarr") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/olaris/olaris.zip") do
    it { should_not exist }
  end

  describe file("/home/testuser/apps/sonarr/Sonarr.exe") do
    it { should be_file }
    it { should be_owned_by "testuser" }
    # it { should be_executable }
  end

  describe file("/etc/apache2/users/testuser/sonarr.conf") do
    it { should be_file }
  end

  describe package("mediainfo") do
    it { should be_installed }
  end
end
