require 'spec_helper'
require 'aethernal_agent'

describe "Configuring a Nzbget user" do
  bundle_update

  describe command('sudo /vagrant/bin/run-aa-for-test nzbget remove_app_user testuser') do
    its(:exit_status) { should eq 0 }
  end

  describe command('sudo /vagrant/bin/run-aa-for-test nzbget configure_app_user testuser \'{"port": 3245}\'') do
    its(:exit_status) { should eq 0 }
  end

  describe port(3245) do
    it { should be_listening }
  end

  describe file("/home/testuser/.config/nzbget") do
    it { should be_directory }
  end

  describe file("/home/testuser/.config/systemd/user/nzbget.service") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/nzbget") do
    it { should be_directory }
    it { should be_owned_by "testuser" }
  end

  describe file("/home/testuser/apps/nzbget/nzbget") do
    it { should be_file }
    it { should be_owned_by "testuser" }
  end

  describe file("/etc/apache2/users/testuser/nzbget.conf") do
    it { should be_file }
  end
end
