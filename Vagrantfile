# -*- mode: ruby -*-
# vi: set ft=ruby :

Vagrant.configure(2) do |config|
  config.vm.box = "ubuntu/bionic64"
  config.vm.network "forwarded_port", guest: 22, host: 22
  config.vm.network "forwarded_port", guest: 80, host: 80
  config.vm.network "forwarded_port", guest: 4567, host: 4567
  config.vm.network "private_network", ip: "10.0.0.123"

  config.vm.provider "virtualbox" do |vb|
    vb.memory = "2048"
    vb.cpus = 4
    disk_file = File.expand_path("../.vagrant/disks/c-disk1.vdi", __FILE__)

    if ! File.exist?(disk_file)
      vb.customize ['createhd', '--filename', disk_file, '--size', 1024 * 1024]
      vb.customize ['storageattach', :id, '--storagectl', 'SATA controller', '--port', 1, '--device', 0, '--type', 'hdd', '--medium', disk_file]
    end
  end

  config.vm.provision :shell, privileged: true, inline: <<-SHELL
       apt update
       apt install -y build-essential libsqlite3-dev snapd apt-transport-https ca-certificates curl gnupg-agent software-properties-common
       snap install ruby --classic
       curl -fsSL https://download.docker.com/linux/ubuntu/gpg > gpg
       apt-key add gpg
       sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
       apt install -y docker-ce docker-ce-cli containerd.io libssl-dev
       adduser --add_extra_groups --gecos '' --quiet --disabled-password testuser
       usermod -a -G docker testuser
  SHELL

  config.vm.provision :reload

  config.trigger.after [:up, :reload] do |trigger|
    trigger.info = "Doing bundle install"
    trigger.run_remote = {inline: "cd /vagrant && /snap/bin/bundle install --system", privileged: true}
  end
end
