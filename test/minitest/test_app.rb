require "minitest/autorun"
require 'aethernal_agent'
require File.expand_path(File.join(File.dirname(__FILE__), 'fixtures/foo/foo_app'))

describe AethernalAgent::App do
  before do
    @app = FooApp.new
  end

  describe "faulty manifest" do
    it "should be true if the aethernal_agent version is high enough" do
      _(@app.check_manifest(@app.manifest)).must_equal true
    end

    it "should be false if the aethernal_agent version is not high enough" do
      @app.manifest.required_aethernal_agent_version = "~> 2.0.0"
      _(@app.check_manifest(@app.manifest)).must_equal false
    end
  end

  describe "should generate options" do
    it "must generate a valid and open port" do
      opts = HashWithIndifferentAccess.new({})
      opts, errors = @app.ensure_action_options(:configure_app_user, opts)
      _(opts).must_include(:port)
      _(opts[:port]).must_be_instance_of(Integer)
      _(opts[:port]).must_be :>=, 0
    end

    it "must generate a valid and open port" do
      opts = HashWithIndifferentAccess.new({})
      opts, errors = @app.ensure_action_options(:configure_app_user, {port: ""})
      _(opts).must_include(:port)
      _(opts[:port]).must_be_instance_of(Integer)
      _(opts[:port]).must_be :>=, 0
    end

    it "must generate a valid and open port between a min and a max if given" do
      opts = HashWithIndifferentAccess.new({})
      opts, errors = @app.ensure_action_options(:configure_app_user, opts)
      _(opts).must_include(:port)
      _(opts[:port]).must_be_instance_of(Integer)
      _(opts[:port]).must_be :>=, 5900
      _(opts[:port]).must_be :<=, 5999
    end

    it "must accept a given port and not touch it if given" do
      opts = HashWithIndifferentAccess.new({port: 7000})
      opts, errors = @app.ensure_action_options(:configure_app_user, opts)
      _(opts).must_include(:port)
      _(opts[:port]).must_equal 7000
    end

    it "must generate a valid random string" do
      opts = HashWithIndifferentAccess.new({})
      opts, errors = @app.ensure_action_options(:configure_app_user, opts)
      _(opts).must_include(:password)
      _(opts[:password]).must_be_instance_of(String)
      _(opts[:password].size).must_be :>=, 0
    end

    it "it shouldnt generate a password if a password is given" do
      options = HashWithIndifferentAccess.new({password: 'password'})
      opts, errors = @app.ensure_action_options(:configure_app_user, options)
      _(opts).must_include(:password)
      _(opts[:password]).must_be_instance_of(String)
      _(opts[:password]).must_equal 'password'
    end
  end

  describe "should smartly parse action manifest" do
    it "must respond with an error if a required option is not given" do
      opts, errors = @app.ensure_action_options(:configure_app_user, {})
      _(errors['user'].size).must_equal 1
      _(errors['user'].first).must_equal 'is required but has not been supplied'
    end

    it "must respond with an error if a required option is not given" do
      opts, errors = @app.ensure_action_options(:configure_app_user, {empty_string: ""})
      _(errors.has_key?('empty_string')).must_equal true
      _(errors['empty_string'].size).must_equal 1
      _(errors['empty_string'].first).must_equal 'is required but has not been supplied'
    end

    it "must respond with an error if a linux user is required but does not exist" do
      opts, errors = @app.ensure_action_options(:configure_app_user, HashWithIndifferentAccess.new({user: 'neo'}))
      _(errors['user'].size).must_equal 1
      _(errors['user'].first).must_equal "can't find user for neo"
    end

    it "must have no errors if everything is supplied locally" do
      opts, errors = @app.ensure_action_options(:configure_app_user, HashWithIndifferentAccess.new({user: 'root', empty_string: "hallo"}))
      _(errors.size).must_equal 0
    end

    it "must have no errors if everything is supplied globally" do
      @app = FooApp.new({user: 'global_user'})
      opts, errors = @app.ensure_action_options(:configure_app_user, {})
      _(errors['user'].size).must_equal 1
      _(errors['user'].first).must_equal "can't find user for global_user"
    end
  end

end
