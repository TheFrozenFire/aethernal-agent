require "minitest/autorun"
require 'aethernal_agent'

AethernalAgent.change_log_level(Logger::INFO)

class ParseTest
  include AethernalAgent::Systemd
  SYSTEMCTL_OUTPUT_RUNNING_ENABLED = "● vnc.service - Remote desktop service (VNC)
   Loaded: loaded (/home/maran/.config/systemd/user/vnc.service; enabled; vendor preset: enabled)
   Active: active (running) since Mon 2019-11-11 10:35:43 UTC; 11min ago
  Process: 21937 ExecStartPre=/bin/sh -c /usr/bin/vncserver -kill :30 > /dev/null 2>&1 || : (code=exited, status=0/SUCCESS)
 Main PID: 21950 (Xtightvnc)"

  SYSTEMCTL_OUTPUT_STOPPED = "● olaris.service - Olaris Media Server
   Loaded: loaded (/home/testuser/.config/systemd/user/olaris.service; disabled; vendor preset: enabled)
   Active: inactive (dead)"

   SYSTEMCTL_OUTPUT_STOPPED_BUT_ENABLED = "● olaris.service - Olaris Media Server
   Loaded: loaded (/home/testuser/.config/systemd/user/olaris.service; enabled; vendor preset: enabled)
   Active: inactive (dead) since Tue 2019-11-26 14:43:47 UTC; 54s ago
  Process: 30194 ExecStart=/home/testuser/apps/olaris/bin/olaris serve --port 5555 (code=killed, signal=TERM)
 Main PID: 30194 (code=killed, signal=TERM)"

  SYSTEMCTL_OUTPUT_FAILED_TO_START = "● vnc.service - Remote desktop service (VNC)
     Loaded: loaded (/home/peter/.config/systemd/user/vnc.service; disabled; vendor preset: enabled)
   Active: failed (Result: exit-code) since Tue 2020-02-11 15:27:19 CET; 18h ago
   Main PID: 6006 (code=exited, status=203/EXEC)"

  def parse_running
    parse_systemd_status_text(SYSTEMCTL_OUTPUT_RUNNING_ENABLED)
  end

  def parse_stopped
    parse_systemd_status_text(SYSTEMCTL_OUTPUT_STOPPED)
  end

  def parse_stopped_but_enabled
    parse_systemd_status_text(SYSTEMCTL_OUTPUT_STOPPED_BUT_ENABLED)
  end

  def parse_failed_to_start
    parse_systemd_status_text(SYSTEMCTL_OUTPUT_FAILED_TO_START)
  end
end

describe ParseTest do
  describe "parsing systemd stati" do
    it "should generate an object based on an active and running process" do
      parse = ParseTest.new
      s = parse.parse_running
      _(s.active).must_equal true
      _(s.enabled).must_equal true
      _(s.running).must_equal true
      _(s.since).must_equal "Mon 2019-11-11 10:35:43 UTC"
    end

    it "should generate an object based on a stopped process" do
      parse = ParseTest.new
      s = parse.parse_stopped
      _(s.active).must_equal false
      _(s.enabled).must_equal false
      _(s.running).must_equal false
    end

    it "should show as enabled even when stopped" do
      parse = ParseTest.new
      s = parse.parse_stopped_but_enabled
      _(s.active).must_equal false
      _(s.enabled).must_equal true
      _(s.running).must_equal false
      _(s.since).must_equal "Tue 2019-11-26 14:43:47 UTC"
    end

    it "should fail to start" do
      parse = ParseTest.new
      s = parse.parse_failed_to_start
      _(s.active).must_equal false
      _(s.enabled).must_equal false
      _(s.running).must_equal false
      _(s.since).must_equal "Tue 2020-02-11 15:27:19 CET"
    end
  end
end
