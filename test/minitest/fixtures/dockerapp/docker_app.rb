class DockerApp < AethernalAgent::App
  def initialize(options = {})
    super(options.merge(file_path: __FILE__))
  end
end
