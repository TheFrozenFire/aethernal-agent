module AethernalAgent
  module Filesystem
    def files_folder
      File.join(File.expand_path(File.dirname(plugin_path)), "files/")
    end

    def files_path(file)
      File.join(files_folder, file)
    end

    def meta_folder
      File.join(File.expand_path(File.dirname(plugin_path)), "meta/")
    end

    def meta_path(file)
      File.join(meta_folder, file)
    end

    def templates_folder
      File.join(File.expand_path(File.dirname(plugin_path)), "templates/")
    end

    def template_path(file)
      File.join(templates_folder, file)
    end

    def home_folder_path(path = nil)
      if !path.nil?
        File.join(Dir.home(self.user), path)
      else
        File.join(Dir.home(self.user))
      end
    end

    def set_ownership(path, user, group=nil)
      AethernalAgent.logger.debug("Setting ownership of '#{path}' to user '#{user}'")
      begin
        FileUtils.chown_R(user,group,path)
      rescue SystemCallError => e
        add_errors(e, path: path, user:user, group: group)
      end
    end

    def set_permissions(path, chmod)
      AethernalAgent.logger.debug("Setting chmod of '#{path}' to chmod '#{chmod}'")
      begin
        FileUtils.chmod(chmod, path)
      rescue SystemCallError => e
        add_errors(e, path: path, chmod: chmod)
      end
    end

    def file_settings(file_path, options = {})
      AethernalAgent.logger.debug "Setting file settings with options: #{options}"
      set_permissions(file_path, options[:chmod]) if options[:chmod]
      set_ownership(file_path, options[:owner]) if options[:owner]
    end

    def file(file_path, options = {})
      options.reverse_merge!(action: :create)

      AethernalAgent.logger.debug "Applying action #{options[:action]} on file '#{file_path}'."

      if options[:action] == :delete
        if File.directory?(file_path)
          add_errors("File is directory, not going to remove it with file() method.", path: file_path)
        elsif File.exist?(file_path)
          begin
            FileUtils.rm(file_path)
          rescue SystemCallError => e
            add_errors(e, path: file_path)
          end
        else
          #add_errors("File does not exist.", path: file_path)
          AethernalAgent.logger.warn "File '#{file_path}' did not exist so not removing"
        end
      else
        set_ownership(file_path, options[:owner]) if options[:owner]
        set_permissions(file_path, options[:chmod]) if options[:chmod]
      end
    end

    def copy(source, target, options = {})
      AethernalAgent.logger.debug "Applying action #{options[:action]} on file or directory '#{source}'."
      if source.present? && target.present?
        if File.directory?(source) || File.exist?(source)
          if File.exists?(target)
            add_errors("File or folder already exists.", path: target)
          else
            begin
              if File.directory?(source)
                FileUtils.cp_r(source, target)
              else
                FileUtils.cp(source, target)
              end
            rescue SystemCallError => e
              add_errors(e, path: file_path)
            end
          end
        else
          add_errors("File or folder does not exist.", path: source)
        end
      else
        add_errors("Source or destination are missing.")
      end
    end

    def directory(path, options = {})
      options.reverse_merge!(action: :create)

      AethernalAgent.logger.debug "Applying action #{options[:action]} on directory '#{path}'."
      if options[:action] == :create
        unless File.directory?(path)
          begin
            FileUtils.mkdir_p(path)
          rescue Errno::EEXIST, Errno::EACCES => e
            add_errors(e, path: path)
            return
          end
        else
          AethernalAgent.logger.debug "#{path} folder already exist, not creating."
        end

        set_ownership(path, options[:owner]) if options[:owner]
        set_permissions(path, options[:chmod]) if options[:chmod]
      elsif options[:action] == :delete
        if File.directory?(path)
          begin
            FileUtils.rm_r(path)
          rescue Errno::EACCES => e
            add_errors(e, path: path)
          end
        else
          AethernalAgent.logger.debug "#{path} does not exist, not removing."
        end
      end

      return true
    end

    def aethernal_agent_folder
    end

    def aethernal_agent_file(source_file, target_file_path, options = {})
      AethernalAgent.logger.debug("Attempting to copy file '#{source_file}' to '#{target_file_path}'")

      if File.exists?(target_file_path)
        AethernalAgent.logger.debug("File already exists, not copying.")
      else
        begin
          FileUtils.cp(File.join(files_folder,source_file), target_file_path)
        rescue SystemCallError => e
          AethernalAgent.logger.debug("'#{source_file}' could not be created, giving up. Error: '#{e}'")
          add_errors(e,source_file: source_file, target_file_path: target_file_path)
          return false
        end
      end

      set_permissions(target_file_path, options[:chmod]) if options[:chmod]
      set_ownership(target_file_path, options[:owner]) if options[:owner]
    end

    def write_template(in_path, out_path, vars, options={})
      out_folder = File.dirname(out_path)
      unless Dir.exist?(out_folder)
        FileUtils.mkdir_p(out_folder)
        set_permissions(out_folder, options[:chmod]) if options[:chmod]
        set_ownership(out_folder, options[:owner]) if options[:owner]
      end

      AethernalAgent.logger.debug("Writing template '#{in_path}' to '#{out_path}'")
      template = AethernalAgent::Template.new(in_path,vars, options)

      if template.parse
        return template.write_to(out_path)
      end
    end
  end
end
