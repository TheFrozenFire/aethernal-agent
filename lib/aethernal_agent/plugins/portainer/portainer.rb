class AethernalAgent::Portainer < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def configure_app_user(options = {})
    install_packages(options)
    res = super(options) do |opts|
      @opts = opts
    end

    if res[:errors].any?
      return create_return_args(options.merge(res[:errors]))
    end

    if !wait_on_port(@opts["port"])
      self.remove_app_user(@opts)
      self.add_errors("Portainer never properly started")
    else
      uri = URI("http://localhost:#{@opts["port"]}/api/users/admin/init")
      req = Net::HTTP::Post.new(uri)
      req.content_type = 'application/json'
      req.body = {username: @opts[:user], password: @opts[:password]}.to_json
      Net::HTTP.start(uri.host, uri.port) do |http|
        response = http.request(req)
        jp =  JSON.parse(response.body)
        if jp["Username"] == @opts[:user]
          AethernalAgent.logger.info("fininshed setting up user")
        else
          AethernalAgent.logger.warn("failed to setup user: #{jp}")
          self.add_errors("Something went wrong while setting up the admin user for Portainer: '#{jp}'")
        end
      end
    end

    return create_return_args(@opts)
  end
end
