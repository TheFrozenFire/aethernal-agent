class AethernalAgent::Sickchill < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user
    super(options)
    run_as('root',"mv #{app_path}/SickChill-2020.06.20-2/* #{app_path}/")
    run_as('root',"pip install #{app_path}/requirements.txt")
    directory(app_path, owner: @user)
    directory(app_path('SickChill-2020.06.20-2'), action: :delete)
    directory(app_path, owner: self.user)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(app_path, action: :delete)
      directory(sickchill_config_path, action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      @vars = {
        password: opts['password'],
        port: opts['port'],
        user: self.user}

      write_template(template_path('config.ini.erb'),
                   sickchill_config_path('config.ini'),
                   @vars,
                   {owner: self.user})

      file(sickchill_config_path('config.ini'), owner: self.user)
    end

    return create_return_args(@vars)
  end

  protected

  def sickchill_config_path(path ="/")
    File.join(home_folder_path(".config/sickchill"), path)
  end

end
