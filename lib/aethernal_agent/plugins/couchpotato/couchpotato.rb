class AethernalAgent::Couchpotato < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user
    super(options)
    directory(app_path, owner: self.user)
    directory(couchpotato_config_path, owner: self.user)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(home_folder_path("/apps/couchpotato"), action: :delete)
      directory(home_folder_path("/.config/couchpotato"), action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      @vars = {
        port: opts['port'],
        download_path: home_folder_path("torrents/watch"),
        password: opts['password'],
        user: self.user}

      write_template(template_path('settings.conf.erb'),
                   couchpotato_config_path('config.ini'),
                   @vars,
                   {owner: self.user})
    end

    return create_return_args(@vars)
  end

  protected

  def couchpotato_config_path(path ="/")
    File.join(home_folder_path(".config/couchpotato"), path)
  end

end
