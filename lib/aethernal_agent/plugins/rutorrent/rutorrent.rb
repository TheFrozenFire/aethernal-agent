class AethernalAgent::Rutorrent < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    super(options)
  end

  def configure_app_user(options = {})
    install_packages(options)

    super do |opts|

      # Setup rc for custom port
      conf_path = File.join(opts[:config_mount], "rtorrent/")
      directory(conf_path, owner: self.user)
      write_template(template_path('rtorrent.rc.erb'),
                     File.join(conf_path, ".rtorrent.rc"),
                     opts,
                     {owner: opts[:user]})


      # Setup authentication
      directory(opts[:password_mount], owner: @user)

      ["rpc.htpasswd", "rutorrent.htpasswd", "webdav.htpasswd"].each do |file_name|
        run_command("htpasswd -cbB #{opts[:password_mount]}/#{file_name} #{@user} #{opts[:password]}")
      end
    end
  end

  def uninstall_packages(options = {})
    super(options)
  end

  def remove_app_user(options = {})
    uninstall_packages(options)
    super(options)
  end
end
