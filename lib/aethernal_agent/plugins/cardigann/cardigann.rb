class AethernalAgent::Cardigann < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user
    super(options)
    directory(app_path, owner: self.user)
    directory(cardigann_config_path, owner: self.user)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(home_folder_path("/apps/cardigann"), action: :delete)
      directory(home_folder_path("/.config/cardigann"), action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      @vars = {
        port: opts['port'],
        api_key: SecureRandom.hex(16),
        password: opts['password'],
        user: self.user}

      write_template(template_path('config.json.erb'),
                   cardigann_config_path('config.json'),
                   @vars,
                   {owner: self.user})
    end

    return create_return_args(@vars)
  end

  protected

  def cardigann_config_path(path ="/")
    File.join(home_folder_path(".config/cardigann"), path)
  end

end
