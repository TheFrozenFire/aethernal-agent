class AethernalAgent::Medusa < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user
    super(options)
    directory(app_path, owner: @user)
    run_as('root',"mv #{app_path}/Medusa-0.3.16/* #{app_path}/")
    directory(app_path('Medusa-0.3.16'), action: :delete)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(app_path, action: :delete)
      directory(medusa_config_path, action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      @vars = {
        password: opts['password'],
        download_path: home_folder_path("torrents/data"),
        watch_path: home_folder_path("torrents/watch"),
        port: opts['port'],
        user: self.user}

      write_template(template_path('config.ini.erb'),
                     medusa_config_path('config.ini'),
                     @vars,
                     {owner: @vars[:user]})
    end

    return create_return_args(@vars)
  end

  protected

  def medusa_config_path(path ="/")
    File.join(home_folder_path(".config/medusa"), path)
  end

end
