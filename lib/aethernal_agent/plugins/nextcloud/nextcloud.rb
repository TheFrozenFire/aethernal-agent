class AethernalAgent::Nextcloud < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    options[:home] = home_folder_path
    remove_app_user
    super(options)
    directory(home_folder_path("/www"), action: :create)
    FileUtils.mv(app_path + "/nextcloud", nextcloud_folder_path)
    file(nextcloud_folder_path, chmod: 711, owner: self.user)
    directory(app_path + "/nextcloud", action: :delete)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(home_folder_path("/www/nextcloud"), action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      @vars = {
        password: opts['password'],
        domain_name: opts['domain_name'],
        user: self.user}
    end

    run_command("cd #{nextcloud_folder_path} && sudo -u #{self.user} /usr/bin/php occ maintenance:install --admin-user=#{self.user} --admin-pass=\"#{@vars[:password]}\"")
    run_command("sed -ibak \"s/0 => 'localhost',/0 => 'localhost',1 => '#{self.user}.#{@vars[:domain_name]}',/\" #{nextcloud_folder_path}/config/config.php")

    return create_return_args(@vars)
  end

  protected

  def nextcloud_folder_path
    home_folder_path("/www/nextcloud")
  end

end
