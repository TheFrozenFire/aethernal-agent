class AethernalAgent::Jdownloader < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user
    super(options)
    run_as('root',"mv #{app_path}/jdownloader/* #{app_path}/")
    directory(app_path, owner: @user)
    directory(jdownloader_config_path, owner: @user)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(app_path, action: :delete)
      directory(jdownloader_config_path, action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      write_template(template_path('org.jdownloader.api.myjdownloader.MyJDownloaderSettings.json.erb'),
                     "#{app_path}/cfg/org.jdownloader.api.myjdownloader.MyJDownloaderSettings.json",
                     opts.merge(server_name: get_global_config(:container_name)),
                     {owner: self.user})
    end
  end

  protected

  def jdownloader_config_path(path ="/")
    File.join(home_folder_path(".config/jdownloader"), path)
  end

end
