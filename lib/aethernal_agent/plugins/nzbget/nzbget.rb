class AethernalAgent::Nzbget < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user
    super(options)
    run_as(self.user,"sh #{app_path}/nzbget-21.0-bin-linux.run")
    directory(app_path, action: :delete)
    run_as("root","mv #{home_folder_path}/nzbget* #{app_path}")
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(app_path, action: :delete)
      directory(nzbget_config_path, action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)
    super(options) do |opts|
      @vars = {
        password: opts['password'],
        port: opts['port'],
        user: self.user}

      write_template(template_path('nzbget.conf.erb'),
                     nzbget_config_path('nzbget.conf'),
                     @vars,
                     {owner: @vars[:user]})
    end

    return create_return_args(@vars)
  end

  protected

  def nzbget_config_path(path ="/")
    File.join(home_folder_path(".config/nzbget"), path)
  end

end
