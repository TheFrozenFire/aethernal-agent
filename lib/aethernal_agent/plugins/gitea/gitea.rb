class AethernalAgent::Gitea < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    super(options)
    file(gitea_path, chmod: 711, owner: self.user)
    file(File.join(gitea_path, 'gitea'), chmod: 711, owner: self.user)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(gitea_config_path, action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      directory(home_folder_path(".config/gitea/"), owner: self.user)

      vars = {
        user: self.user,
        home_folder: home_folder_path,
        port: opts[:port],
        ssh_port: opts[:ssh_port],
        ssh_host_port: opts[:ssh_host_port],
        container_name: get_global_config(:container_name),
        host_name: get_global_config(:hostname)
      }.merge(generated_tokens_from_gitea)

      write_template(template_path('app.ini.erb'),
                     gitea_config_path('app.ini'),
                     vars,
                      {
                        owner: vars[:user]
                      }
                    )
    end
  end

  protected
  def gitea_path
    app_path('gitea')
  end

  def gitea_config_path(path ="/")
    File.join(home_folder_path(".config/gitea"), path)
  end

  def generated_tokens_from_gitea
    internal_token = `#{gitea_path} generate secret INTERNAL_TOKEN`
    secret_key = `#{gitea_path} generate secret SECRET_KEY`
    lfs_jwt_secret = `#{gitea_path} generate secret LFS_JWT_SECRET`

    {
      internal_token: internal_token,
      secret_key: secret_key,
      lfs_jwt_secret: lfs_jwt_secret
    }
  end

end
