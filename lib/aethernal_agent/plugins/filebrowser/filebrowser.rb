class AethernalAgent::Filebrowser < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    super(options)
    file(filebrowser_path, chmod: 711, owner: self.user)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(home_folder_path(".config/filebrowser"), action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      run_as(self.user,"#{filebrowser_path} config init -d #{filebrowser_db_path}")
      run_as(self.user,"#{filebrowser_path} users add #{self.user} #{opts[:password]} --perm.admin -d #{filebrowser_db_path}")
      run_as(self.user,"#{filebrowser_path} config set -p #{opts[:port]} -a '[::]' -b '/filebrowser' -d #{filebrowser_db_path}")
      run_as(self.user,"#{filebrowser_path} rules add -r \"[\\\\\\/]\\..+\" -u #{self.user} -d #{filebrowser_db_path}")
    end
  end

  protected
  def filebrowser_path
    app_path('filebrowser')
  end

  def filebrowser_db_path
    File.join(home_folder_path(".config/filebrowser"), "filebrowser.db")
  end

  def source_path(opts)
    File.join(filebrowser_path, source_name, 'filebrowser')
  end

  def source_name
    self.manifest.package['direct_download']['target_name']
  end

end
