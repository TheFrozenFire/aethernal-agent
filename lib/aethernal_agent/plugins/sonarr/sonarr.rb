class AethernalAgent::Sonarr < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user

    super(options)
    file(app_path, chmod: 711, owner: self.user)
    FileUtils.mv(app_path + "/Sonarr", app_path + "/sonarr")
    FileUtils.mv(app_path + "/sonarr", home_folder_path(File.join("apps")))
    directory(app_path, action: :delete)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(home_folder_path("/apps/sonarr"), action: :delete)
      directory(home_folder_path("/apps/Sonarr"), action: :delete)
      directory(home_folder_path("/.config/Sonarr"), action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)
    super(options) do |opts|
      @vars = {
        api_key: Digest::SHA256.hexdigest(Time.now.to_i.to_s + "aethernal")[0..32],
        port: opts['port'],
        ssl_port: opts['ssl_port'],
        password: opts['password'],
        user: self.user}

      write_template(template_path('config.xml.erb'),
                     sonarr_config_path('config.xml'),
                     @vars,
                     {owner: @vars[:user]})
    end
    # If we run this within the super we have no systemd scripts yet so we need to do this after everything has been setup except the user.
    add_user_to_db
    return create_return_args(@vars)
  end

  protected

  def sonarr_config_path(path ="/")
    File.join(home_folder_path(".config/Sonarr"), path)
  end

  def add_user_to_db
    # We need to start it for the database and migrations to be created
    start
    # Let's sleep a bit so we have some guarantee that the database has been migrated properly
    sleep 120
    # Let's stop it so we can mess with the database, not sure this is needed
    stop
    begin
      db = SQLite3::Database.open(home_folder_path(".config/Sonarr/sonarr.db"))
      # Add default user
      db.execute "INSERT INTO Users VALUES (1, 'e852a6bb-6812-4e6c-8f99-f8c3c5201594', '#{self.user}', '#{Digest::SHA256.hexdigest(@vars[:password])}');"
    rescue SQLite3::Exception => e
      raise "Error setting up Sonarr: #{e}"
    ensure
      db.close if db
    end

    start
  end

end
