class AethernalAgent::Syncthing < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    super(options)
  end

  def uninstall_packages(options = {})
    super(options)
  end

  def remove_app_user(options = {})
    uninstall_packages(options)
    super(options) do
      directory( home_folder_path(".config/syncthing"), owner: self.user, action: :delete)
    end
  end

  def configure_app_user(options = {})
    install_packages(options)

    super(options) do |opts|
      directory( home_folder_path(".config/syncthing"), owner: self.user)
      write_template(template_path('config.xml.erb'),
                     home_folder_path(".config/syncthing/config.xml"),
                     opts.merge(hash: BCrypt::Password.create(opts[:password], cost: 4).to_s, api_key: SecureRandom.hex(32)),
                     {owner: self.user})
    end
  end

end
