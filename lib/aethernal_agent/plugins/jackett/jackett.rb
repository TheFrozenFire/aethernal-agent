class AethernalAgent::Jackett < AethernalAgent::App
  def initialize(options = {})
    super(options)
  end

  def install_packages(options = {})
    remove_app_user
    super(options)
    run_as('root',"mv #{app_path('Jackett')}/* #{app_path}/")
    directory(app_path('Jackett'), action: :delete)
  end

  def remove_app_user(options = {})
    super(options) do |opts|
      directory(app_path, action: :delete)
      directory(jackett_config_path, action: :delete)
    end
  end

  def configure_app_user(options = {})
    self.install_packages(options)

    super(options) do |opts|
      api_key = SecureRandom.hex(16)
      sha_password = Digest::SHA2.new(512).hexdigest("#{opts['password']}#{api_key}".encode("UTF-16LE"))

      @vars = {
        password: opts['password'],
        api_key: api_key,
        sha_password: sha_password,
        port: opts['port'],
        user: self.user}

      write_template(template_path('ServerConfig.json.erb'),
                     jackett_config_path('ServerConfig.json'),
                     @vars,
                     {owner: @vars[:user]})
    end

    return create_return_args(@vars)
  end

  protected

  def jackett_config_path(path ="/")
    File.join(home_folder_path(".config/Jackett"), path)
  end

end
