module AethernalAgent
  module Docker
    class Env
      attr_accessor :key, :value
      def initialize(key, value)
        self.key= key
        self.value= value
      end

      def self.create_from_options(manifest_options, opts)
        envs = []
        manifest_options.each do |k,v|
          if v.present?
            if v["docker"].present? && v["docker"]["env"].present?
              if v["docker"].has_key?("auto_source") && ["user_id"].include?(v["docker"]["auto_source"])
                # For now only allow user_id to be auto-sourced but we can add more things later
                 user_id = Etc.getpwnam(opts["user"]).uid
                envs << Env.new(v["docker"]["env"], user_id)
              else
                envs << Env.new(v["docker"]["env"], opts[k])
              end
            end
          end
        end
        return envs
      end

      def to_docker
        "#{self.key}=#{self.value}"
      end
    end
  end
end
