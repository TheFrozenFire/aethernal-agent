require 'aethernal_agent/apt'
require 'aethernal_agent/utils'
require 'aethernal_agent/apache/apache'

module AethernalAgent
  class Loader
    include AethernalAgent::Apt
    include AethernalAgent::Utils

    attr_accessor :loaded_manifests

    def initialize
      STDOUT.sync = true
      self.loaded_manifests = []
      self.dynamic_load
    end

    def ensure_server_state
      apt_package(packages: ["unzip", "apache2", "build-essential", "libsqlite3-dev", "sqlite3", "libapache2-mpm-itk"])

      AethernalAgent::Apache.ensure_default_config
    end

    def dynamic_load
      AethernalAgent.logger.info "Checking embedded plugins"
      embedded_plugins = File.expand_path(File.join(File.dirname(__FILE__), 'plugins/*/*.yml'))
      require_plugins_from_paths(embedded_plugins)

      AethernalAgent.logger.info "Checking custom plugins"
      custom_plugins = File.join(Dir.home,".config", "bytesized", "aethernal_agent", "plugins", "*", "*.yml")
      require_plugins_from_paths(custom_plugins)

      AethernalAgent.logger.info "Ensuring server state"
      self.ensure_server_state
    end

    def require_plugins_from_paths(paths)
      AethernalAgent.logger.debug "Checking in #{paths}"
      Dir[paths].each do |file_path|
        m = read_manifest(file_path)
        AethernalAgent.logger.info "Loading #{m.name} - v#{m.version} from #{m.script_name}"
        if check_manifest(m)
          require file_path.gsub("manifest.yml",m["script_name"])
          self.loaded_manifests << m
        else
          AethernalAgent.logger.warn "Manifest #{m.name} required aethernal_agent version '#{m.required_aethernal_agent_version}' but '#{AethernalAgent::VERSION}' is loaded, not loading plugin."
        end
      end
    end

    def read_manifest(file_path)
      AethernalAgent.logger.info "Loading manifest from '#{file_path}'"
      return OpenStruct.new(YAML.load_file(file_path))
    end
  end
end
