require 'pathname'

module AethernalAgent
  module Cli
    class Services < Thor
      attr_accessor :user
      include AethernalAgent::Utils

      desc "start APP", "start the supplied application via systemd"
      def start(app)
        run_systemd_plain("start", app)
      end

      desc "stop APP", "stop the supplied application via systemd"
      def stop(app)
        run_systemd_plain("stop", app)
      end

      desc "restart APP", "restart the supplied application via systemd"
      def restart(app)
        run_systemd_plain("restart", app)
      end

      desc "status APP", "get the status of the supplied application via systemd"
      def status(app)
        result = run_systemd_plain("status", app)
        puts result
      end

      desc "start-all", "Start all systemd managed applications"
      def start_all
        puts "Starting all known services"
        service_files = `ls ~/.config/systemd/user/*.service`.split("\n")
        result = service_files.collect do |s|
          pn = Pathname.new(s)
          app_name = pn.basename
          puts "Starting #{app_name}"
          run_systemd_plain("start", app_name)
        end
      end

      desc "list", "Lists all available user services and their status."
      def list
        service_files = `ls ~/.config/systemd/user/*.service`.split("\n")
        result = service_files.collect do |s|
          pn = Pathname.new(s)
          app_name = pn.basename
          status = systemd_text_for_service(app_name).split("\n")[2].strip
          [app_name, status]
        end
        table = Terminal::Table.new :headings => ['Service', 'Status'], :rows => result
        puts table
      end
    end
  end
end
