require 'aethernal_agent/cli/services'

module AethernalAgent
  module Cli

    class Core < Thor
      desc "services [SUBCOMMAND]", "Get Application status"
      subcommand "services", AethernalAgent::Cli::Services
    end
  end
end
