require 'aethernal_agent/filesystem'
require 'aethernal_agent/utils'

module AethernalAgent
  class Apache
    include AethernalAgent::Filesystem
    include AethernalAgent::Utils
    include AethernalAgent::Errors
    extend AethernalAgent::Utils

    attr_accessor :user

    def self.ensure_default_config
      run_command("a2enmod rewrite proxy scgi headers proxy proxy_html proxy_http ssl xml2enc proxy_wstunnel mpm_itk")
      run_command("echo 'ProxyPass /aethernal http://127.0.0.1:4567/' > /etc/apache2/sites-available/aa-default.conf")
      run_command("a2ensite aa-default")
      run_command("systemctl restart apache2")
    end

    def initialize(options = {})
      self.user = options['user'] || 'root'
    end

    def write_app_config(template_path, app_name, port)
      directory(apache_user_conf_path)
      write_template(template_path, apache_user_conf_path(app_name), {port: port})

      reload
    end

    def remove_app_config(app_name)
      file(apache_user_conf_path(app_name), action: :delete)
    end

    def ensure_base_config(options = {})
      config_file = File.join(File.expand_path(File.dirname(__FILE__)), "templates","aa-user.conf.erb")

      container_name = get_global_config(:container_name)
      host_name = get_global_config(:hostname)

      add_errors("hostname not found in global config") unless host_name
      add_errors("container_name not found in global config") unless container_name

      write_template(config_file, "/etc/apache2/sites-available/aethernal_agent-#{self.user}.conf",{
        home_folder: home_folder_path,
        user: self.user,
        container_name: container_name,
        host_name: host_name,
        custom_domains: options[:custom_domains]
      })

      directory(home_folder_path("www"), owner: self.user)

      run_command("a2ensite aethernal_agent-#{self.user}")

      return nil
    end

    def reload
      run_command("systemctl reload apache2")
    end

    def restart
      run_command("systemctl restart apache2")
    end

    def apache_user_conf_path(app_name = nil)
      path = File.join("/etc/apache2/users", self.user)
      path = File.join(path, "#{app_name}.conf") if app_name
      path
    end
  end
end
